INCLUDE_DIRS = -L $(LDINC)

Q ?= @
 
CC = $(CC_PATH)arm-none-eabi-gcc

CXX = $(CC_PATH)arm-none-eabi-g++
CXXFLAGS = $(COMPILE_OPTS) $(INCLUDE_DIRS)

AS = $(CC_PATH)arm-none-eabi-gcc
ASFLAGS = $(COMPILE_OPTS) -c

CSTANDARD = -std=gnu99
	
MCFLAGS = -mcpu=cortex-m0
	
CFLAGS = -mthumb -gdwarf-2 -mfloat-abi=soft
CFLAGS += $(CSTANDARD)
CFLAGS += $(MCFLAGS)
CFLAGS += -ffunction-sections -fdata-sections 
CFLAGS += -O0 -g3 -Wall -c -MMD $(addprefix -I,$(INC)) $(addprefix -D,$(DEF))

LD = $(CC_PATH)arm-none-eabi-gcc
LDFLAGS = $(MCFLAGS) -mthumb -Wl,--gc-sections,-Map=$@.map,-cref,-u,Reset_Handler $(INCLUDE_DIRS) $(LIBRARY_DIRS) -T$(LDSCRIPT)

OBJCP = $(CC_PATH)arm-none-eabi-objcopy
OBJCPFLAGS = -O binary

AR = $(CC_PATH)arm-none-eabi-ar
ARFLAGS = cr

#
%.o: %.s Makefile $(LDSCRIPT)
	@echo 'Compiling $(notdir $<)'
	$(Q)$(CC) $(CFLAGS) -o$@ $<

%.o: %.c Makefile $(LDSCRIPT)
	@echo 'Compiling $(notdir $<)'
	$(Q)$(CC) $(CFLAGS) -o$@ $<
	
%.elf: $(obj-y) $(LDSCRIPT) 
	@echo 'Linking $@'
	$(Q)$(CC) $(LDFLAGS) $(obj-y) --output $@ 
	