ifeq (,$(filter obj%,$(notdir $(CURDIR)))) 
include target.mk 
else 
#----- End Boilerplate 

MAIN_OUT = stm32f0_gpio

ifeq ($(_ARCH),stm32)
CFG_ARCH_STM32 := y
MAIN_OUT_ELF = $(MAIN_OUT).elf
else
ifeq ($(_ARCH),linux)
CFG_ARCH_LINUX := y
MAIN_OUT_ELF = $(MAIN_OUT).aout
endif
endif

# all
.SECONDARY:
all: $(MAIN_OUT_ELF)

.PHONY:
clean:

# App files
INC = $(SRCDIR)
VPATH += $(SRCDIR)

# ST standard peripheral library
STD_PERIPH_LIB = $(SRCDIR)/STM32F0xx_StdPeriph_Lib_V1.5.0/Libraries
VPATH += $(STD_PERIPH_LIB)/STM32F0xx_StdPeriph_Driver/src
VPATH += $(STD_PERIPH_LIB)/CMSIS/Device/ST/STM32F0xx/Source/Templates/gcc_ride7

# Device selection
DEF += STM32F051     
DEF += USE_STDPERIPH_DRIVER

# Use asserts to trap errors.
DEF += USE_FULL_ASSERT

# Application files
obj-$(CFG_ARCH_STM32) += main.o
obj-$(CFG_ARCH_STM32) += stm32f0xx_it.o
obj-$(CFG_ARCH_STM32) += system_stm32f0xx.o
obj-$(CFG_ARCH_STM32) += startup_stm32f051.o

# ST Cube HAL driver files
obj-$(CFG_ARCH_STM32) += stm32f0xx_rcc.o
obj-$(CFG_ARCH_STM32) += stm32f0xx_gpio.o

# Host tests
obj-$(CFG_ARCH_LINUX) += test.o
obj-$(CFG_ARCH_LINUX) += stubs.o
 
$(MAIN_OUT_ELF): $(obj-y) Makefile rules_gcc_stm32.mk

# List defines here
INC += $(STD_PERIPH_LIB)/STM32F0xx_StdPeriph_Driver/inc
INC += $(STD_PERIPH_LIB)/CMSIS/Device/ST/STM32F0xx/Include
INC += $(STD_PERIPH_LIB)/CMSIS/Include

# List include directories here

# Linker script
LDSCRIPT += $(SRCDIR)/STM32F051R8_FLASH.ld
LDINC += $(SRCDIR)

# Dependancy handling
-include $(patsubst %.o,%.d,$(obj-y))

# Tool invocations
include $(SRCDIR)/rules_gcc_$(_ARCH).mk
	
#----- Begin Boilerplate 
endif